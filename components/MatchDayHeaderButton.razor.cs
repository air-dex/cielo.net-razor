/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Razor.
///
/// Cielo.NET Razor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Razor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Razor. If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Cielo.Razor {
	public partial class MatchDayHeaderButton: IRootClass {
		public string RootClass { get => "matchday-headerbutton"; }

		[Parameter]
		public IEnumerable<string> OtherClasses { get; set; }

		IEnumerable<string> RootClasses {
			get {
				LinkedList<string> list = new LinkedList<string>(OtherClasses ?? new LinkedList<string>());
				list.AddFirst("btn");
				list.AddFirst(RootClass);

				return list;
			}
		}

		[Parameter]
		public bool Disabled { get; set; }

		[Parameter]
		public EventCallback<MouseEventArgs> OnClick { get; set; }

		[Parameter]
		public RenderFragment ChildContent { get; set; }
	}
}

