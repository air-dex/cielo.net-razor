/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Razor.
///
/// Cielo.NET Razor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Razor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Razor. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;

namespace Cielo.Razor {
	public class RankingCellImpl: ComponentBase, IRootClass, IRankingCell {
		public string RootClass { get => "ranking-cell"; }

		[Parameter]
		public IEnumerable<string> OtherClasses { get; set; }

		[Parameter]
		public string CellElt { get; set; }

		[Parameter]
		public string CellClass { get; set; }

		[Parameter]
		public RenderFragment ChildContent { get; set; }

		[Parameter]
		public string RankingRowRootClass { get; set; } = RankingRow.ROOT_CLASS;

		public IEnumerable<string> RootClasses {
			get {
				LinkedList<string> list = new LinkedList<string>(OtherClasses ?? new LinkedList<string>());
				list.AddFirst(RootClass);

				if (RankingRowRootClass != null && CellClass != null) {
					list.AddFirst($"{RankingRowRootClass}__{CellClass}-cell");
				}

				return list;
			}
		}

		protected override void BuildRenderTree(RenderTreeBuilder __builder) {
			__builder.OpenElement(0, CellElt);

			Dictionary<string, object> attr = new Dictionary<string, object>();
			attr.Add("class", (this as IRootClass).CssClasses);
			attr.Add("data-cielo-razor", true);
			__builder.AddMultipleAttributes(1, attr);

			if (ChildContent != null) {
				__builder.AddContent(2, ChildContent);
			}

			__builder.CloseElement();
		}
	}
}
