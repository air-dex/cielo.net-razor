/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Razor.
///
/// Cielo.NET Razor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Razor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Razor. If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using Microsoft.AspNetCore.Components;
using Cielo.Core;

namespace Cielo.Razor {
	public partial class RankingLine: IRootClass {
		public static readonly string ROOT_CLASS = "ranking-line";

		public string RootClass { get => ROOT_CLASS; }

		[Parameter]
		public IEnumerable<string> OtherClasses { get; set; }

		[Parameter]
		public int Position { get; set; }

		[Parameter]
		public Ranking Ranking { get; set; }

		[Parameter]
		public int PtsPrecision { get; set; } = 3;
	}
}
