/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Razor.
///
/// Cielo.NET Razor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Razor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Razor. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Cielo.Core;

namespace Cielo.Razor {
	public partial class MatchDayHeader<Tday>: IRootClass {
		public string RootClass { get => "matchday-header"; }

		[Parameter]
		public IEnumerable<string> OtherClasses { get; set; }

		IEnumerable<string> RootClasses {
			get {
				LinkedList<string> list = new LinkedList<string>(OtherClasses ?? new LinkedList<string>());
				list.AddFirst("btn-toolbar");
				list.AddFirst(RootClass);

				return list;
			}
		}

		// Index management

		[Parameter]
		public int CurrentIndex { get; set; }

		public EventCallback<int> CurrentIndexChanged { get; set; }

		public async Task goStart() {
			CurrentIndex = 0;
			await CurrentIndexChanged.InvokeAsync(CurrentIndex);
		}

		public async Task goPrev() {
			CurrentIndex--;
			await CurrentIndexChanged.InvokeAsync(CurrentIndex);
		}

		public async Task goNext() {
			CurrentIndex++;
			await CurrentIndexChanged.InvokeAsync(CurrentIndex);
		}

		public async Task goEnd() {
			CurrentIndex = Days.Count -1;
			await CurrentIndexChanged.InvokeAsync(CurrentIndex);
		}

		public bool StartDisabled {
			get => DaysEmpty || CurrentIndex == 0;
		}

		public bool EndDisabled {
			get => DaysEmpty || CurrentIndex == DaysCount -1;
		}

		// Days management

		[Parameter]
		public IList<Tday> Days { get; set; }

		protected bool DaysEmpty {
			get => Days == null || Days.Count == 0;
		}

		protected int DaysCount {
			get => DaysEmpty ? 0 : Days.Count;
		}

		protected string DayToString(Tday day) {
			return Days[CurrentIndex].ToString();
		}

		protected string DayToString(int day) {
			return $"{day}ème journée";
		}

		protected string DayToString(DateTime day) {
			return day.ToString("dd/MM/YYYY");
		}
	}
}

