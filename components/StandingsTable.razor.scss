/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Razor.
///
/// Cielo.NET Razor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Razor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Razor. If not, see <http://www.gnu.org/licenses/>.

$_bgcolor-even: whitesmoke;
$_bgcolor-odd: white;
$_ranking-row-classname: ranking-row;
$_header-border: (
	color: slategray,
	width: 3px
);
$_row-border: (
	color: lightgray,
	width: 1px
);

@mixin border-bottom($width: 0px, $style: solid, $color: black) {
	border-bottom: {
		width: $width;
		style: $style;
		color: $color;
	}
}

.standings-table {
	overflow-y: auto;

	&__header {
		@include border-bottom(
			$width: map-get($_header-border, 'width'),
			$color: map-get($_header-border, 'color')
		);

		font-size: larger;
	}

	&__body {
		> .ranking-row {
			&:nth-child(even) {
				background-color: $_bgcolor-even;
			}

			&:nth-child(odd) {
				background-color: $_bgcolor-odd;
			}

		}

		> :not(:last-child) {
			@include border-bottom(
				$width: map-get($_row-border, 'width'),
				$color: map-get($_row-border, 'color')
			);
		}
	}

	// Counter Bootstrap
	margin: 0px;
	padding: 0px;

	td, th {
		border: none;
	}
}
