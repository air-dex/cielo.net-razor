/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Razor.
///
/// Cielo.NET Razor is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Razor is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Razor. If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using Microsoft.AspNetCore.Components;

namespace Cielo.Razor {
	public partial class RankingRow: IRootClass {
		public static readonly string ROOT_CLASS = "ranking-row";

		public string RootClass { get => ROOT_CLASS; }

		[Parameter]
		public IEnumerable<string> OtherClasses { get; set; }

		[Parameter]
		public string CellElt { get; set; }

		[Parameter]
		public RenderFragment RankCell { get; set; }

		[Parameter]
		public RenderFragment TeamCell { get; set; }

		[Parameter]
		public RenderFragment MatchesCell { get; set; }

		[Parameter]
		public RenderFragment WinsCell { get; set; }

		[Parameter]
		public RenderFragment DrawsCell { get; set; }

		[Parameter]
		public RenderFragment LossesCell { get; set; }

		[Parameter]
		public RenderFragment GoalsForCell { get; set; }

		[Parameter]
		public RenderFragment GoalsAgainstCell { get; set; }

		[Parameter]
		public RenderFragment GoalsDifferenceCell { get; set; }

		[Parameter]
		public RenderFragment PtsCell { get; set; }
	}
}
